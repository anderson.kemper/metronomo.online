const metronome = document.querySelector(".metronome");
const startStopButton = document.getElementById("startStopButton");
const bpmSlider = document.getElementById("bpmSlider");
const bpmDisplay = document.getElementById("bpmDisplay");
let isPlaying = false;
let intervalId;
let audioContext = new (window.AudioContext || window.webkitAudioContext)();
let tickSound;

// Carregar o som do tick (bip) usando um oscilador simples
function loadTickSound() {
    tickSound = audioContext.createOscillator();
    tickSound.frequency.setValueAtTime(440, audioContext.currentTime); // Frequência do beep
    tickSound.type = 'sine'; 
    tickSound.connect(audioContext.destination);
}

// Na API Web Audio, o atributo type define a forma da onda que um oscilador irá gerar. Existem alguns tipos de formas de onda disponíveis. Além do "square" (quadrada), que gera um som de "beep", há outras formas que você pode usar para criar diferentes timbres. Aqui estão alguns dos tipos de formas de onda mais comuns disponíveis em osciladores na API Web Audio:
// Square: Gera uma onda quadrada.
// Sine (senoidal): Gera uma forma de onda senoidal, que é suave e possui um timbre puro, frequentemente usada para instrumentos como flauta ou voz.
// Sawtooth (dente de serra): Gera uma forma de onda dente de serra, que é rica em harmônicos e pode ter um som brilhante ou áspero.
// Triangle (triangular): Gera uma forma de onda triangular, que é mais suave do que a dente de serra, mas mais rica em harmônicos do que a senoidal.
// Custom (personalizada): Além dos tipos padrão acima, a API Web Audio permite criar formas de onda personalizadas usando uma tabela de forma de onda (waveform table), que permite definir seus próprios pontos da forma de onda.
// Essas formas de onda podem ser usadas para criar diferentes timbres e texturas sonoras. Experimentar com diferentes tipos de forma de onda e combinações pode ser uma maneira interessante de explorar a criação de sons musicais.

function playTick() {
    loadTickSound();
    tickSound.start();
    metronome.style.backgroundColor = "red";
    setTimeout(() => {
        metronome.style.backgroundColor = "#333";
        tickSound.stop();
    }, 100);
}

function updateBpmDisplay(value) {
    bpmDisplay.textContent = "BPM: " + value;
}

function toggleMetronome() {
    if (isPlaying) {
        clearInterval(intervalId);
        startStopButton.textContent = "Iniciar";
    } else {
        const bpm = parseInt(bpmSlider.value);
        intervalId = setInterval(playTick, 60000 / bpm);
        startStopButton.textContent = "Parar";
    }
    isPlaying = !isPlaying;
}

// Adicionar um ouvinte de evento para capturar alterações no slider
bpmSlider.addEventListener("input", function () {
    const bpm = parseInt(bpmSlider.value);
    updateBpmDisplay(bpm); // Atualizar o valor exibido
    if (isPlaying) {
        clearInterval(intervalId);
        intervalId = setInterval(playTick, 60000 / bpm);
    }
});

startStopButton.addEventListener("click", toggleMetronome);
